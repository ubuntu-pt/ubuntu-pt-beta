---
title: "TRADUCAO_EN:Contacto para Privacidade"
description: "TRADUCAO_EN:Informação sobre contacto para dúvidas e exercício de Direitos sobre Dados Pessoais"
---

TRADUCAO_EN:Se deseja contactar a organização da Ubucon Portugal 2023, com dúvidas sobre questões de privacidade deste sítio Web, questões de privacidade no próprio evento, ou exercício de Direitos legais sobre os seus dados pessoais, pode [utilizar este formulário]({{< var/dataProtectionLink >}}).

TRADUCAO_EN:Não se esqueça também de consultar a nossa ([política de privacidade](/privacy-policy/)).