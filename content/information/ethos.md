---
title: "Ethos"
description: "Código de conduta, regras, princípios e políticas da COLOCAR TEXTO"
---

A {{< var/siteName >}} é regida por regras; elas existem para garantir a segurança e o bem-estar de todos, proporcionando a melhor experiência possível.

Sendo um evento da Comunidade Ubuntu Portugal e sendo esta comunidade aderente ao [Código de Conduta do Ubuntu](https://ubuntu.com/community/ethos/code-of-conduct), este deve ser observado por todos, pelo que convidamos à sua leitura atenta.

Além do Cógido de Conduta do Ubuntu, também nos regimos pela sua [política de diversidade](https://ubuntu.com/community/ethos/diversity) e pela sua [missão](https://ubuntu.com/community/ethos/mission).

A respeito de privacidade, um tema que também nos é bastante caro, temos uma política de privacidade ([podem consultar aqui](/privacy-policy/)), que explica como gerimos a privacidade dos visitantes do nosso sítio Web e participantes do evento.