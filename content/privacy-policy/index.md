---
title: "Política de Privacidade da COLOCAR TEXTO"
description: "Aplicada desde COLOCAR TEXTO (ex: 9 de Julho, 2023)"
---

# A organização da {{< var/siteName >}}

A **Equipa de organização da {{< var/siteName >}}** de agora em diante refereida como 'a equipa', ou simplesmente 'equipa' estabelece e comunica esta política de privacidade nos termos que se seguem para garantir que todos os visitantes do website da Ubucon Portugal e participantes do evento estão informados a respeito dos seus Direitos, e dos dados pessoais que recolhemos e dos fins para os quais os processamos adicionalmente, e que outras entidades os poderão estar a processar e qual o fim para o qual o fazem.

> Esta política de privacidade é aplicávei desde dia COLOCAR TEXTO (ex: 9 de Julho, 2023).

Versões anteriores desta política de privacidade podem ser encontradas no [repositório de código fonte do website da Ubucon Europe 2023](https://gitlab.com/ubuntu-pt/Ubucon-pt-2023).

A equipa é constituída por indivíduos voluntários, sem que haja qualquer tipo de relação contratual entre eles, entre eles e a Canonical, COLOCAR SE TIVER MAIS.

A equipa não é uma entidade legalmente constituída, apenas um grupo informal de indivíduos, alguns que se consideram membros da Comunidade Ubuntu Portugal, outros sem qualquer filiação.

Apenas os membros equipa que são simultâneamente Adminstradores da Comunidade Ubuntu Portugal, que estão envolvidos na administração dos seus meios técnicos necessários e/ou na organização da {{< var/siteName >}}, vão ter acesso aos dados pessoais recolhidos com o compromisso de não serem tornados públicos, pelos periodos também identificados nesta política de privacidade.


# Processamento de dados pessoais de visitantes e utilizadores do website da {{< var/siteName >}}

A equipa do nosso website não recolhe dados pessoais dos visitantes do website da {{< var/siteName >}}, excepto no caso dos vistantes que:
1. Utilizem o [formulário de contacto](/contact/geral/)
2. Utilizem o [formulário de exercício de direitos relativos a dados pessoais](/contact/direitos_dados_pessoais/)


## Identificação dos dados pessoais recolhidos, dos fundamentos legais

| Dados pessoais | Fundamentos Legais | Propósito | Período de retenção | Obrigação de fornecer | Dados serão publicados | Notas adicionais |
| -------------- | ------------------ | --------- | ------------------- | --------------------- | ---------------------- | ---------------- |
| Nome Alcunha pública | - | Identificação do participante perante o público do evento e divulgação do evento, organização dao evento, incluido recepção, planeamento e divulgação de propostas de actividades submetidas pelos sujeitos dos dados | indefinidamente | é obrigatório apenas para quem submete propostas de actividades | Dado será tornado público | a escolha de utilização nome ou alcunha pública é do sujeito dos dados |
| Launchpad Id   | - | Identificação do participante perante o público do evento e divulgação do evento, organização dao evento, incluido recepção, planeamento e divulgação de propostas de actividades submetidas pelos sujeitos dos dados | indefinidamente | não é obrigatório | Dado será tornado público | - |
| Link para a página do Wiki do Ubuntu | - | identificação do participante perante o público do evento e divulgação do evento, organização dao evento, incluido recepção, planeamento e divulgação de propostas de actividades submetidas pelos sujeitos dos dados | indefinidamente |não é obrigatório | Dado será tornado público | - |
| Dados Biográficos | - | Fornecer aos participantes e público informação para os ajudar a decidir sobre a importância em assistir à actividade realizada pelo sujeito dos dados, na Ubucon Portugal 2023, organização dao evento, incluido recepção, planeamento e divulgação de propostas de actividades submetidas pelos sujeitos dos dados | indefinidamente | é obrigatório apenas para quem submete propostas de actividades | Os dados serão tornados público | os dados fornecidos são inteiramente da escolha do sujeito dos dados |
| Endereço de email | - | contacto entre a organização e o sujeito de dados, para fins da organização do evento, comunicação de decisões da organização, fornecimento de informações para o sujeito necessárias à sua participação, responder a questões e pedidos de exercício de direitos legais | 6 meses | é obrigatório em alguns casos | Este dado não será tornado público | a obrigação de fornecimento deste dado só existe para os indivíduos que submetam propostas de actividades, utilizem o formulário de contactos e/ou exerçam pedidos relativos aos seus direitos como sujeitos de dados de acordo com a legislação em vigor |
| Número de telefone móvel | - | contacto entre a organização e o sujeito de dados, para fins da organização do evento, comunicação de decisões da organização, fornecimento de informações para o sujeito necessárias à sua participação | 6 meses | é obrigatório em alguns casos | Este dado não será tornado público |  a obrigação de fornecimento deste dado só existe para os indivíduos que submetam propostas de actividades, utilizem o formulário de contactos e/ou exerçam pedidos relativos aos seus direitos como sujeitos de dados de acordo com a legislação em vigor |

### Sobre os dados processados de forma indefinida:
Os dados dados processados de forma indefinida servem para o propósito de promover o evento, incluíndo em futuras edições, assegurar uma memória pública do evento, e serão publicados no website da {{< var/siteName >}}, em futuras versões de arquivo realizadas e publicadas pela equipa da organização da {{< var/siteName >}}, pela Comunidade Ubuntu Portugal, ou quaisquer terceiros que o façam.


## Processamento de dados por terceiros ao visitar e utilizar o website da {{< var/siteName >}}

### Processamento por fornecedores de infraestrutura de alojamento do website

#### GitLab B.V.

O website da Ubucon Europe 2023, está alojado na infraestrutura da [GitLab B.V.](https://about.gitlab.com/company/) utilizando o serviço [GitLabs Pages](https://about.gitlab.com/direction/plan/knowledge/pages/#gitlab-pages), e a GitLab B.V. realiza algum processamento de dados. Podem saber mais como como o GitLab faz processamento e de que dados, para quais fins na sua [Política de Privacidade](https://about.gitlab.com/privacy), mas fazemos notar as seguintes utilizações, que julgamos serem as relevantes para os visitantes de páginas alojadas com GitLabs Pages:

* Entender como os seus serviços são utilizados e como os podem melhorar;
* Para melhorar a segurança dos serviços
* Para detectar abusos
* Para exercer os seus termos de utilização
* Para cumprir com obrigações legais
* Para proteger os direitos dos visitantes e garantir protecção e propriedade do GitLab ou de terceiros
* Para os objectivos para os quais o visitante conceda consentimento informado

Adicionalmente recomendamos também a visita e leitura da [página do website da GitLab B.V., a respeito do cumprimento do Regulamento Geral de Protecção de Privacidade](https://about.gitlab.com/privacy/privacy-compliance/) e também a [página da equipa de Privacidade](https://about.gitlab.com/handbook/legal/privacy/).

A GitLab B.V. é uma empresa constituída nos Países Baixos.

#### Comunidade Ubuntu Portugal

A Comunidade Ubuntu Portugal, oferece à equipa, uma instância de Nextcloud utilizada para todos os formulários nos quais são recolhidos todos os tipos de dados submetidos pelos utilizadores, a saber:

1. O [formulário de contacto]({{< var/contactLink >}})
2. O [formulário de exercício de direitos relativos a dados pessoais]({{< var/dataProtectionLink >}})


#### Hetzner Online GmbH

A Comunidade Ubuntu Portugal, recorre aos serviços da [Hetzner Online GmbH](https://www.hetzner.com/unternehmen/ueber-uns?country=pt), para alojar a sua instância de Nextcloud (utilizada para processar dados referidos na secção sobre a Comunidade Ubuntu Portugal)

De acordo com a sua [política de privacidade](https://www.hetzner.com/legal/privacy-policy), a Hetzner Online GmbH, não recolhe dados pessoais de pessoas que não sejam seus clientes e por isso não processa dados dos visitantes e utilizadores do sítio web da Ubucon Europe que se limitem ao contexto de visitar e utilizar esse mesmo sítio web.

A Hetzner Online GmbH é uma empresa constituída na República Federal Alemã.


#### Ubuntu/Canonical

O sítio web da {{< var/siteName >}}, recorre a uma versão alojada pela Canonical à [Vanilla Framework](https://vanillaframework.io/), uma framework de CSS, utilizada no sítio web para definir a aparência visual das páginas do sítio web.
Podem consultar o seu código fonte no [seu repositório do GitHub](https://github.com/canonical/vanilla-framework), e podem consultar a [política de privacidade da Canonical](https://ubuntu.com/legal/data-privacy).

## Sobre destruição dos dados pessoais após o período de retenção

Os membros da equipa vão destruir os dados armazenados na Nextcloud e quaisquer cópias locais que tenham, após o período de retenção de dados.

## Quais sãos os seus Direitos como sujeito dos dados?

Para mais informação sobre quais os seus Direitos, visite o sítio web da [Comissão Nacional de Protecção de Dados](https://www.cnpd.pt/cidadaos/direitos/)

## Como pode exercer os seus Direitos como sujeitos dos dados

Para exercer os seus Direitos enquanto sujeito de dados utilize o [formulário que criámos especificamente para o efeito](/contact/direitos_dados_pessoais/).
