---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
externalRedirect: {{ getenv "HUGO_EXTERNAL_URL" }}
---

A redirecionar para a página {{ getenv "HUGO_EXTERNAL_URL" }} ...

[Pressione para redirecionar manualmente]({{ getenv "HUGO_EXTERNAL_URL" }})
