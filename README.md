@João Luz: Para tirares ideias
https://github.com/gohugoio/hugoThemes#themetoml

# Site baseado na framework Hugo e Vanilla framework

[Hugo](https://gohugo.io)


[Vanilla framework](https://vanillaframework.io/)

# Instalar via LXD

##Instalar LXD 
[Instalar LXD](https://canonical.com/lxd/install)

(Configurações por omissão servem)

##Criar 'Container'
```bash
lxc launch ubuntu: ubuntu-pt-site
```

##Actualizar o sistema
```bash
apt update && apt full-upgrade && snap refresh && reboot
```

# Instalar o Hugo
```bash
snap install hugo --channel=extended/stable
```

# Clonar repositorio

## Gerar Chave ssh e colocar no gitlab

### Gerar Chave ssh
```bash
ssh-keygen
```
### Obter a chave
cat ~/.ssh/id_ed25519.pub 

Algo assim:
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMLtx9YSsJsW9435DDSFCktF5/W0sOmsf1+1cZy4/XXXX centrolinux@centrolinux-LT1000

### Colocar no gitLab
Colocar o link para a docuimentação


## Instalar git
```bash
apt install git-all
```


## Colonar repsoitorio
git clone git@gitlab.com:ubuntu-pt/ubuntu-hugo-template.git


Navegar para a directoria 

# Arrancar com o Hugo
```bash
hugo server --logLevel debug
```

# Arrancar com o Hugo Para aceder remotamente
```bash
hugo serve --bind <Ip do Container> --baseURL http://<Ip do Container> -p 1313
```
Exemplo:

```bash
hugo serve --bind 10.203.115.177 --baseURL http://10.203.115.177 -p 1313
```

# Arrancar com o Hugo em DEV
```bash
./run_dev.sh
```

# Arrancar com o Hugo em PRD
```bash
./run_prd.sh
```

# Link para o site



# Criar página genérica
```bash
hugo new <Local e nome da página>
```
Exemplo:

```bash
hugo new about/about.md
```

# Criar página para redirecionar
```bash
HUGO_EXTERNAL_URL="<Link externo>" hugo new --kind <Idioma>/externalRedirect <Local e nome da página><Idioma>.md
```
Exemplo:

```bash
HUGO_EXTERNAL_URL="https://ubuntu.com" hugo new --kind pt/externalRedirect about/ubuntu.md
```



# Criar conteudo estático com o Hugo na pasta public
```bash
hugo
```

# Shortcodes Templates
[shortcode-templates](https://gohugo.io/templates/shortcode-templates/)

Quando se criar novos shortcodes ter a preocupação de os fazer com Acessibilidade
[ARIA](https://www.w3.org/WAI/ARIA/apg/)

### timedisplay
Formata a data conforme o idioma do teu browser

Como usar no markdown:

```bash
 {{< timedisplay datetime="2022-08-28T23:55:00+01:00" display="date" >}}
```

Como usar directamente no HTML:

```html
 <u class="timedisplay" display="date" style="text-decoration-style: dotted;">2022-08-28T23:55:00+01:00</u></p>
```

Opções no parametro "display":
- date
- time
- qualquer coisa => Retorna datetime

### accordion
Expandir ou recolher uma secção (pode ser texto)

Como usar no markdown:

```bash
{{< accordion title="<Titulo do acordeão>" id="<Id Unico>" expanded="<true|false>" >}}
	<O que fica dentro do acordeão>
{{< /accordion >}}
```
Exemplo:

```bash
{{< accordion title="Comunidade Ubuntu Portugal" id="idCUP" expanded="false" >}}
	Este exemplo de acordeão veio do git Comunidade Ubuntu Portugal Template
{{< /accordion >}}
```

### button
Botão que suporta mini icon a esquerda

```bash
{{< button text="<Texto do Botão>" href="<link do botão>" icon="<icon do botão>" >}}
```
Exemplo:

```bash
{{< button text="Botão com icon de Link Externo" href="https://ubuntu-pt.org/" icon="external-link" >}}
```

### profile
Template para profile

```bash
{{< profile fotografia="<Fotografia>" nome="<Nome>" bold="<Frase que quero destacadar>" desc="<Aqui é uma mini descrição>" >}}
```
Exemplo:

```bash
{{< profile fotografia="/images/logo.simplificado.svg" nome="Princeps Civium" bold="O primeiro dos cidadãos" desc="A nova estrutura política criada por Augusto designa-se por \"principado\", sendo o chefe do império designado por princeps civium" >}}
```




# Alterações necessárias nos novos projectos

### Ficheiro config.yaml

Todas as que fizerem sentido alterar

### Ficheiro i18n/pt-pt.toml

Na secção "Especifico"


### Ficheiro /static/CNAME

Colocar o Domínio.
Exemplo:

```bash
 ubuconpt2023.ubuntu-pt.org
```

### Imagem /static/images/og/ogimage.png
Alterar a imagem que se quer usar como fundo na imagem que vai ser usada nas redes socias


### Ficheiro content/information/about.md

Na descrição no cabeçalho


### Ficheiro content/privacy-policy/index.md

Procurar por "COLOCAR TEXTO"


### Ficheiro layouts/index.html

Procurar por "COLOCAR TEXTO"

### Em todo o site

Procurar por "TRADUCAO_EN:"


#og_img_generator
Open Graph image generator => Gerador de imagens para serem usadas nas redes socias

[opengraph](https://www.opengraph.xyz/) => Para vizualisar o resultado

[The Open Graph protocol](https://ogp.me/) => Regras do Open Graph

### Indicações

### Instalar python3
Construção 
	-> Instalar o modulo imgkit
	
	```bash
	sudo apt install wkhtmltopdf
	```
	 


#Cores que usamos no site
```bash
 #b5185f => ~Vermelho violeta médio
 #700c69 => ~Roxo
 #270073 => ~Índigo
```



# Sintaxe básica do readme.md
[Sintaxe básica](https://docs.github.com/pt/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax#quoting-text)


# License
Code: MIT, Contents: CC BY 4.0