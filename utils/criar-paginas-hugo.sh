#!/bin/bash
set -o nounset

if [ $# -eq 1 ] ; then
	idioma=$1
else
	idioma=""
fi

cd ..

for linha in $(grep 'url' config.yaml | grep --invert-match '#' | grep --invert-match '\-group$' | awk -F: '{ print $2 }' | awk '{$1=$1;print}'); do
	#echo "Linha = $linha"
	if [[ $idioma = "" ]]; then
		echo hugo new "${linha}.md" 
		hugo new "${linha}.md" 
	else 
		echo hugo new "${linha}.${idioma}.md" 
		hugo new "${linha}.${idioma}.md" 
	fi
done
